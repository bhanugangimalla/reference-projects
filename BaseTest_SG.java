package Unilever.Unilever;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;

import com.google.gson.JsonObject;
import com.mongodb.client.model.Filters;


import config.Config;
import utils.DateUtil;
import utils.DocDbDao;
import utils.ExcelOperations;
import utils.JSWaiter;
import utils.ReporterUtil;
import utils.TestListener;

@Listeners(TestListener.class)
public class BaseTest {

	public Capabilities capabilities;
	// public RemoteWebDriver driver;
	protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();
	
	public ReporterUtil reporterUtil;
	// public Object[][] dataConfig;
	public  SoftAssert softAssert;
	public String App_URL;
	public String strEnv;

	@BeforeSuite(alwaysRun=true)
	public void suiteSetup(ITestContext context) {
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		Config.ENV = System.getProperty("DFOS.env");
		// System.out.println(context.getCurrentXmlTest().getParameter("Env"));
		if (!Config.ENV.equalsIgnoreCase("qa")) {
			
			if (Config.ENV.equalsIgnoreCase("prod")) {
				Config.APP_URL = Config.APP_URL.replace("-qa", "");
			}
			else
			{
				Config.APP_URL = Config.APP_URL.replace("qa", Config.ENV);	
			}
			
			System.out.println("Env " + Config.ENV + " App Url " + Config.APP_URL);
		}
		
		 
		  
		
		 
	}

	@BeforeClass(alwaysRun=true)
	public void setAssert() {
		// softAssert = new SoftAssert();
	}

	@BeforeSuite()
	public void startGrid() {
		startSeleniumGrid();
	}

	@BeforeMethod(alwaysRun=true)
	@Parameters({ "browser" })
	public void setUp(String browsername) {
		try {

			setWebDriver(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), getCapabilities(browsername)));
			getDriver().navigate().to(Config.APP_URL);
		} catch (Exception e) {
			try {
				setWebDriver(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), getCapabilities(browsername)));
				getDriver().navigate().to(Config.APP_URL);
			} catch (MalformedURLException e1) {
			
			}
			
		
		}

		System.out.println("beforeTest with url - " + Config.APP_URL);
	}

	@AfterMethod()
	public void afterTest() {
		
		stopWebDriver();
	}

	@AfterSuite()
	public void tearDown() {
	
		stopWebDriver();
		
	}

	@DataProvider(name = "BaseTest" ,parallel=false)
	public Object[][] dataSupplier() throws IOException {
		//return ExcelOperations.getDatafromSheet(BaseTest.class.getSimpleName());
		return ExcelOperations.getDatafromSheet(Config.ENV);
	}

	public static void setWebDriver(RemoteWebDriver redriver) {
		driver.set(redriver);
	}

	public static WebDriver getDriver() {
		// Get driver from ThreadLocalMap
		return driver.get();
	}

	public Capabilities getCapabilities(String browser) {
		if (browser.equals("firefox"))
			capabilities = getFirefoxOptions();
		else
			capabilities = getChromeOptions();
		return capabilities;
	}

//Get Chrome Options
	public static ChromeOptions getChromeOptions() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		// options.addArguments("--incognito");
		return options;
	}

//Get Firefox Options
	public static FirefoxOptions getFirefoxOptions() {
		FirefoxOptions options = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();
		// Accept Untrusted Certificates
		profile.setAcceptUntrustedCertificates(true);
		profile.setAssumeUntrustedCertificateIssuer(false);
		// Use No Proxy Settings
		profile.setPreference("network.proxy.type", 0);
		// Set Firefox profile to capabilities
		options.setCapability(FirefoxDriver.PROFILE, profile);
		return options;
	}

	public static void startSeleniumGrid() {
		try {
			Runtime.getRuntime().exec("cmd /c start " + Config.SELENIUM_GRID_FILE_PATH);
		} catch (Exception e) {
			// LoggerUtil.logErrorMessage("Could not start Selenium grid: " + e);
		}
	}

	public static void stopWebDriver() {

		try {
			getDriver().close();
		} catch (Exception e) {
		}

		try {
			//getDriver().quit();
		} catch (Exception e) {
		}

	}
	
	public static void killChromeDriver() {
		try { 
			
			Runtime.getRuntime().exec("taskkill /F /IM ChromeDriver.exe");
	 	} 
		catch (Exception e) {
			//LoggerUtil.logErrorMessage("Could not start Selenium grid: " + e);				 
		}
	}	

	
	public static void stopSeleniumGrid() { try {
		  Runtime.getRuntime().exec("taskkill /F /IM cmd.exe"); } catch (Exception e) {
		  //LoggerUtil.logErrorMessage("Could not start Selenium grid: " + e); } }
		  
		  }
	
	}
}
  
  

  

 